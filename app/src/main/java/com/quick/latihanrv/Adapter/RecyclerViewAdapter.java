package com.quick.latihanrv.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quick.latihanrv.R;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    Context mContext;
    final private ItemClickListener mClickListener;
    private ArrayList<String> mMember;
    private ArrayList<String> mStatus;

    public RecyclerViewAdapter(Context context, ItemClickListener mClickListener, ArrayList<String> Name, ArrayList<String> Status) {
        this.mContext = context;
        this.mClickListener = mClickListener;
        this.mMember = Name;
        this.mStatus = Status;
//        this.mReference = Reference;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.list_member, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_grup_member.setText(mMember.get(position));
        String one = mMember.get(position).substring(0, 1);
        one = one.toLowerCase();
        String status = mStatus.get(position);
        if (status.contains("inactive")){
            holder.tv_grup_member.setTextColor(mContext.getResources().getColor(R.color.colorFontSecondary));
            holder.tv_status.setVisibility(View.VISIBLE);
            holder.cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorBackground));
        }else {
            holder.tv_grup_member.setTextColor(mContext.getResources().getColor(R.color.colorFont));
            holder.tv_status.setVisibility(View.GONE);
            holder.setColor(one);
        }
    }

    @Override
    public int getItemCount() {
        return mMember.size();
    }

    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        CardView cv_root;
        private TextView tv_grup_member, tv_status;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_grup_member = (TextView) itemView.findViewById(R.id.tv_member);
            tv_status = (TextView) itemView.findViewById(R.id.tv_status);
            cv_root = (CardView) itemView.findViewById(R.id.cv_nameMember);
            cv_root.setOnClickListener(this);
            cv_root.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            mClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }

        void setColor(String huruf) {
            switch (huruf) {
                case "a":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.a));
                    break;
                case "b":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.b));
                    break;
                case "c":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.c));
                    break;
                case "d":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.d));
                    break;
                case "e":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.e));
                    break;
                case "f":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.f));
                    break;
                case "g":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.g));
                    break;
                case "h":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.h));
                    break;
                case "i":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.i));
                    break;
                case "j":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.j));
                    break;
                case "k":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.k));
                    break;
                case "m":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.m));
                    break;
                case "n":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.n));
                    break;
                case "o":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.o));
                    break;
                case "p":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.p));
                    break;
                case "q":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.q));
                    break;
                case "r":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.r));
                    break;
                case "s":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.s));
                    break;
                case "t":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.t));
                    break;
                case "u":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.u));
                    break;
                case "v":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.v));
                    break;
                case "w":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.w));
                    break;
                case "x":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.x));
                    break;
                case "y":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.y));
                    break;
                case "z":
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.z));
                    break;
                default:
                    cv_root.setCardBackgroundColor(mContext.getResources().getColor(R.color.other));
                    break;
            }
        }
    }

}
