package com.quick.latihanrv.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quick.latihanrv.R;
import com.quick.latihanrv.dbHelper;

import java.util.ArrayList;
import java.util.Arrays;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.ViewHolder> {
    final private ItemClickListener mClickListener;
    dbHelper helper;
    private ArrayList<String> mGrup;
    private ArrayList<String> mJumlah;

    public RvAdapter(Context context, ItemClickListener mClickListener, ArrayList<String> Name, ArrayList<String> Jumlah) {
        this.mClickListener = mClickListener;
        this.mGrup = Name;
        this.mJumlah = Jumlah;
        helper = new dbHelper(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.list_main, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_grup_title.setText(mGrup.get(position));
//        if (mJumlah.size() != 0){
//        }
        holder.tv_grup_member.setText(mJumlah.get(position) + " item");
    }

    @Override
    public int getItemCount() {
        return mGrup.size();
    }

    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        CardView cv_root;
        private TextView tv_grup_title;
        private TextView tv_grup_member;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_grup_title = (TextView) itemView.findViewById(R.id.tv_grup_title);
            tv_grup_member = (TextView) itemView.findViewById(R.id.tv_grup_count);
            cv_root = (CardView) itemView.findViewById(R.id.cv_nameGrup);

            cv_root.setOnClickListener(this);
            cv_root.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            mClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }
    }


}
