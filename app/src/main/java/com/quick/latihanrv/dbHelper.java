package com.quick.latihanrv;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class dbHelper extends SQLiteOpenHelper {
    private static final String TABLE_ITEM = "tb_item";
    private static final String TABLE_DATA = "tb_data";

    // Common column names
    public static final String ID_GRUP = "id_grup";
    public static final String TB1_GRUP_NAME = "grup_name";
    public static final String TB2_GRUP_MEMBER = "grup_member";

    // MEMBER Table - column names
    public static final String ID_MEMBER = "id_member";
    public static final String TB1_KEY_MEMBER = "member_name";
    public static final String TB2_KEY_MEMBER_STATUS = "status";
    public static final String TB3_KEY_MEMBER_GRUP = "id_grup";

    String mQuery;

    public dbHelper(Context context) {
        super(context, "db_data", null, 1);
    }

    private static final String CREATE_TABLE_GRUP = "CREATE TABLE "
            + TABLE_DATA + " ("
            + ID_GRUP+ " INTEGER PRIMARY KEY, "
            + TB1_GRUP_NAME + " TEXT " + ")";

    private static final String CREATE_TABLE_ITEM = "CREATE TABLE "
            + TABLE_ITEM + "("
            + ID_MEMBER + " INTEGER PRIMARY KEY, "
            + TB1_KEY_MEMBER + " TEXT, "
            + TB2_KEY_MEMBER_STATUS + " TEXT, "
            + TB3_KEY_MEMBER_GRUP + " INTEGER" + ")";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_GRUP);
        db.execSQL(CREATE_TABLE_ITEM);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    void insertData(ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DATA, null, values);
        db.close();
    }

    void insertMember(ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_ITEM, null, values);
        db.close();
    }

    Cursor queryGrupName() {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "SELECT * FROM " + TABLE_DATA;
        return db.rawQuery(mQuery, null);
    }

    Cursor queryGrupName2() {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "SELECT tbl1.* , tbl3.jumlah"
        + " FROM "+TABLE_DATA+ " tbl1"
        + " LEFT JOIN(SELECT id_grup, count(id_grup) jumlah"
        + " FROM "+TABLE_ITEM+" group by id_grup) tbl3 " +
                "ON tbl1.id_grup = tbl3.id_grup ORDER BY "+ ID_GRUP +" DESC" ;
        return db.rawQuery(mQuery, null);
    }

    Cursor queryMemberName(String grup_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "SELECT * FROM " + TABLE_ITEM
                + " WHERE " + TB3_KEY_MEMBER_GRUP + " = '" + grup_id + "' ORDER BY " +
                ID_MEMBER + " DESC";
        return db.rawQuery(mQuery, null);
    }

    Cursor queryMemberActive(String grup_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "SELECT * FROM " + TABLE_ITEM
                + " WHERE " + TB3_KEY_MEMBER_GRUP + " = '" + grup_id + "'"
                + " AND " + TB2_KEY_MEMBER_STATUS + " = 'active'"
                + " ORDER BY " + ID_MEMBER + " DESC";
        return db.rawQuery(mQuery, null);
    }

    void updateMember(String id_member, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "UPDATE " + TABLE_ITEM + " " +
                "SET " + TB2_KEY_MEMBER_STATUS + " = '" + status + "' " +
                "WHERE " + ID_MEMBER + " = '" + id_member + "'";
        db.execSQL(mQuery);
        db.close();
    }

    void deleteGrup(String id_grup) {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "DELETE FROM " + TABLE_DATA + " " +
                "WHERE " + ID_GRUP + " = '" + id_grup + "'";
        String mQuery2 = "DELETE FROM " + TABLE_ITEM + " " +
                "WHERE " + ID_GRUP + " = '" + id_grup + "'";
        db.execSQL(mQuery);
        db.execSQL(mQuery2);
        db.close();
    }

    void deleteMember(String id_member) {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "DELETE FROM " + TABLE_ITEM + " " +
                "WHERE " + ID_MEMBER + " = '" + id_member + "'";
        db.execSQL(mQuery);
        db.close();
    }
}

