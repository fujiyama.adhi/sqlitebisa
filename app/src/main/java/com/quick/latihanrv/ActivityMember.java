package com.quick.latihanrv;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.quick.latihanrv.Adapter.RecyclerViewAdapter;
import com.quick.latihanrv.Adapter.RvAdapter;

import java.util.ArrayList;

public class ActivityMember extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener {
    String grup, id_grup;
    ArrayList<String> listStatus = new ArrayList<String>(),
            listMember = new ArrayList<String>(),
            listIdMember = new ArrayList<String>();
    LottieAnimationView  anim_member_kosong;
    dbHelper helper;
    FloatingActionButton fb_add_member;

    private RecyclerView rv_member;
    private RecyclerViewAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);
        helper = new dbHelper(this);

        grup = getIntent().getStringExtra("Grup");
        id_grup = getIntent().getStringExtra("Id_grup");

        setTitle(grup);
        getName();

        anim_member_kosong = (LottieAnimationView) findViewById(R.id.anim_member_kosong);
        rv_member = (RecyclerView) findViewById(R.id.rv_member);
        fb_add_member = (FloatingActionButton) findViewById(R.id.fb_add_member);

        fb_add_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listMember.size() > 0){
                    Intent i = new Intent(ActivityMember.this, ActivityRandom.class);
                    i.putExtra("id_grup", id_grup);

                    startActivity(i);
                }else {
                    Toast.makeText(ActivityMember.this, "Member kosong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rv_member.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rv_member.setItemAnimator(new DefaultItemAnimator());
        rv_member.setLayoutManager(mLayoutManager);
        adapter = new RecyclerViewAdapter(this, this, listMember, listStatus);

        rv_member.setAdapter(adapter);
        kosong();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void kosong(){
        if (listMember.size() > 0){
            anim_member_kosong.setVisibility(View.GONE);
        }else {
            anim_member_kosong.setVisibility(View.VISIBLE);
        }
    }

    void addItem(String member, String id_grup) {
        ContentValues values = new ContentValues();
        values.put(helper.TB1_KEY_MEMBER, member);
        values.put(helper.TB3_KEY_MEMBER_GRUP, id_grup);
        values.put(helper.TB2_KEY_MEMBER_STATUS, "active");
        helper.insertMember(values);
        getName();
//        listMember.add(0, listMember.get(0));
        rv_member.scrollToPosition(0);
        adapter.notifyItemInserted(0);
        kosong();
    }

    void removeItem(String id_member, int index){
        listMember.remove(index);
        helper.deleteMember(id_member);

        adapter.notifyItemRemoved(index);
        kosong();
    }

    void getName() {
        Cursor c = helper.queryMemberName(id_grup);
        listMember.clear();
        listStatus.clear();
        listIdMember.clear();
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String name = c.getString(c.getColumnIndex(helper.TB1_KEY_MEMBER));
                String status = c.getString(c.getColumnIndex(helper.TB2_KEY_MEMBER_STATUS));
                String id_member = c.getString(c.getColumnIndex(helper.ID_MEMBER));

                listMember.add(name);
                listStatus.add(status);
                listIdMember.add(id_member);

                c.moveToNext();

                Log.e("NAME", name);
            }
        }
        Log.e("NAME ID", "" + listMember.size());
    }

    void dialogAdd() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewInflater = inflater.inflate(R.layout.dialog_add, (ViewGroup) findViewById(android.R.id.content), false);
        final EditText et_title = (EditText) viewInflater.findViewById(R.id.et_add_title);
        et_title.setHint("Nama Member");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Masukkan Member")
                .setView(viewInflater)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (et_title.length() == 0) {
                            Toast.makeText(ActivityMember.this, "Masukkan judul", Toast.LENGTH_SHORT).show();
                        } else {
                            String title = et_title.getText().toString();
                            Toast.makeText(ActivityMember.this, title, Toast.LENGTH_SHORT).show();
                            addItem(title, id_grup);

                            dialog.cancel();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    void dialogDelete(final String id_grup, final int position){
        new AlertDialog.Builder(this)
                .setTitle("Delete Item").setMessage("Are you sure want to delete this item?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeItem(id_grup, position);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delete,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        }
        if (item.getItemId() == R.id.menu_add){
            dialogAdd();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (isLongClick){
            dialogDelete(listIdMember.get(position), position);
        }else {
            if (listStatus.get(position).contains("inactive")){
                helper.updateMember(listIdMember.get(position), "active");
            }else {
                helper.updateMember(listIdMember.get(position), "inactive");
            }
            getName();

            adapter.notifyDataSetChanged();
        }

    }
}
