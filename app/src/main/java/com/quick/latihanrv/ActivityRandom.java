package com.quick.latihanrv;

import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class ActivityRandom extends AppCompatActivity {
    String id_grup;
    ImageButton ib_replay, ib_back;
    TextView tv_random;
    dbHelper helper;
    RelativeLayout rl_random;
    Handler handler;
    Runnable run;
    ArrayList<String>
            listMember = new ArrayList<String>(),
            listIdMember = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_random);
        helper = new dbHelper(this);

        rl_random = (RelativeLayout) findViewById(R.id.rl_random);
        tv_random = (TextView) findViewById(R.id.tv_random);
        ib_replay = (ImageButton) findViewById(R.id.ib_replay);
        ib_back = (ImageButton) findViewById(R.id.ib_back);

        id_grup = getIntent().getStringExtra("id_grup");

        ib_replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goAction();
            }
        });

        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        goAction();
    }

    void getName(String id_grup) {
        Cursor c = helper.queryMemberActive(id_grup);
        listMember.clear();
        listIdMember.clear();
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String name = c.getString(c.getColumnIndex(helper.TB1_KEY_MEMBER));
                String id_member = c.getString(c.getColumnIndex(helper.ID_MEMBER));

                listMember.add(name);
                listIdMember.add(id_member);

                c.moveToNext();

                Log.e("NAME", name);
            }
        }
        Log.e("NAME ID", "" + listMember.size());
    }

    void goAction() {
        ib_replay.animate().rotation(1080).scaleX(0f).scaleY(0f).setDuration(500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ib_replay.setVisibility(View.GONE);
            }
        }, 600);

        getName(id_grup);

        handler = new Handler();
        run = new Runnable() {
            @Override
            public void run() {
                tv_random.setText(getRandom(listMember));
                String one = tv_random.getText().toString().substring(0, 1);
                one = one.toLowerCase();
                setColor(one);
                Log.d("on", "fire");
            }
        };

        Runnable expandUp = new Runnable() {
            @Override
            public void run() {
                tv_random.animate().scaleX(3f).scaleY(3f).setDuration(300);
                ib_replay.setVisibility(View.VISIBLE);
                ib_replay.animate().rotation(0).scaleX(1f).scaleY(1f).setDuration(500);
            }
        };

        Runnable expandDown = new Runnable() {
            @Override
            public void run() {
                tv_random.animate().scaleX(1f).scaleY(1f).setDuration(300);

            }
        };

        int delay = 400;
        int controlTime = 400;
        for (int i = 0; i < 10; i++) {
            handler.postDelayed(run, delay);
            delay += (controlTime -= 40);
            Log.d("DELAY increment", "" + delay);
        }

        for (int i = 0; i < 20; i++) {
            handler.postDelayed(run, delay);
            delay += 50;
            Log.d("DELAY puncak", "" + delay);
        }

        for (int i = 0; i < 20; i++) {
            handler.postDelayed(run, delay);
            delay += 25;
            Log.d("DELAY puncak", "" + delay);
        }

        for (int i = 0; i < 20; i++) {
            handler.postDelayed(run, delay);
            delay += 50;
            Log.d("DELAY puncak", "" + delay);
        }

        Random r = new Random();
        int lastSpin = r.nextInt(5);
        lastSpin += 22;

        controlTime = 25;
        for (int i = 0; i < lastSpin; i++) {
            handler.postDelayed(run, delay);
            delay += (controlTime += 20);
            Log.d("DELAY decrement", "" + delay);
        }

        handler.postDelayed(expandUp, delay + 100);
        handler.postDelayed(expandDown, delay + 400);
    }

    String getRandom(ArrayList<String> randArray) {
        Random r = new Random();
        int index = r.nextInt(randArray.size());
        return randArray.get(index);
    }

    void setColor(String huruf) {
        switch (huruf) {
            case "a":
                rl_random.setBackgroundColor(getResources().getColor(R.color.a));
                break;
            case "b":
                rl_random.setBackgroundColor(getResources().getColor(R.color.b));
                break;
            case "c":
                rl_random.setBackgroundColor(getResources().getColor(R.color.c));
                break;
            case "d":
                rl_random.setBackgroundColor(getResources().getColor(R.color.d));
                break;
            case "e":
                rl_random.setBackgroundColor(getResources().getColor(R.color.e));
                break;
            case "f":
                rl_random.setBackgroundColor(getResources().getColor(R.color.f));
                break;
            case "g":
                rl_random.setBackgroundColor(getResources().getColor(R.color.g));
                break;
            case "h":
                rl_random.setBackgroundColor(getResources().getColor(R.color.h));
                break;
            case "i":
                rl_random.setBackgroundColor(getResources().getColor(R.color.i));
                break;
            case "j":
                rl_random.setBackgroundColor(getResources().getColor(R.color.j));
                break;
            case "k":
                rl_random.setBackgroundColor(getResources().getColor(R.color.k));
                break;
            case "m":
                rl_random.setBackgroundColor(getResources().getColor(R.color.m));
                break;
            case "n":
                rl_random.setBackgroundColor(getResources().getColor(R.color.n));
                break;
            case "o":
                rl_random.setBackgroundColor(getResources().getColor(R.color.o));
                break;
            case "p":
                rl_random.setBackgroundColor(getResources().getColor(R.color.p));
                break;
            case "q":
                rl_random.setBackgroundColor(getResources().getColor(R.color.q));
                break;
            case "r":
                rl_random.setBackgroundColor(getResources().getColor(R.color.r));
                break;
            case "s":
                rl_random.setBackgroundColor(getResources().getColor(R.color.s));
                break;
            case "t":
                rl_random.setBackgroundColor(getResources().getColor(R.color.t));
                break;
            case "u":
                rl_random.setBackgroundColor(getResources().getColor(R.color.u));
                break;
            case "v":
                rl_random.setBackgroundColor(getResources().getColor(R.color.v));
                break;
            case "w":
                rl_random.setBackgroundColor(getResources().getColor(R.color.w));
                break;
            case "x":
                rl_random.setBackgroundColor(getResources().getColor(R.color.x));
                break;
            case "y":
                rl_random.setBackgroundColor(getResources().getColor(R.color.y));
                break;
            case "z":
                rl_random.setBackgroundColor(getResources().getColor(R.color.z));
                break;
            default:
                rl_random.setBackgroundColor(getResources().getColor(R.color.other));
                break;
        }
    }
}
