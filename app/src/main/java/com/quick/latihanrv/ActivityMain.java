package com.quick.latihanrv;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.stetho.Stetho;
import com.quick.latihanrv.Adapter.RvAdapter;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;

public class ActivityMain extends AppCompatActivity implements RvAdapter.ItemClickListener {
    FloatingActionButton fb_add;
    LottieAnimationView anim_grup_kosong;
    ArrayList<String> listJumlah = new ArrayList<String>(), listGrup = new ArrayList<String>(), listIdGrup = new ArrayList<String>();
    dbHelper helper;
    private RecyclerView rv_main;
    private RvAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Stetho.initializeWithDefaults(this);
        helper = new dbHelper(this);

        getName();

        rv_main = (RecyclerView) findViewById(R.id.rv_main);
        fb_add = (FloatingActionButton) findViewById(R.id.fb_add);
        anim_grup_kosong = (LottieAnimationView) findViewById(R.id.anim_grup_kosong);

        rv_main.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rv_main.setItemAnimator(new DefaultItemAnimator());
        rv_main.setLayoutManager(mLayoutManager);
        adapter = new RvAdapter(this, this, listGrup, listJumlah);

        rv_main.setAdapter(adapter);
        kosong();

        fb_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAdd();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getName();
        adapter.notifyDataSetChanged();
        kosong();
    }

    void kosong(){
        if (listGrup.size() > 0){
            anim_grup_kosong.setVisibility(View.GONE);
        }else {
            anim_grup_kosong.setVisibility(View.VISIBLE);
        }
    }

    void addItem(String title) {
        ContentValues values = new ContentValues();
        values.put(helper.TB1_GRUP_NAME, title);
        helper.insertData(values);
        getName();
        Log.e("GRUP", title);
        rv_main.scrollToPosition(0);
        adapter.notifyItemInserted(0);
        kosong();
    }

    void removeItem(String id, int index){
        listGrup.remove(index);
        helper.deleteGrup(id);

        adapter.notifyItemRemoved(index);
        kosong();
    }

    void getName() {
        Cursor c = helper.queryGrupName2();
        listGrup.clear();
        listJumlah.clear();
        listIdGrup.clear();
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String id_grup = c.getString(c.getColumnIndex(helper.ID_GRUP));
                String name = c.getString(c.getColumnIndex(helper.TB1_GRUP_NAME));
                String jumlah = c.getString(c.getColumnIndex("jumlah"));

                listGrup.add(name);
                listIdGrup.add(id_grup);
//                listJumlah.add(jumlah);
                if (jumlah == null){
                    listJumlah.add("0");
                } else {
                    listJumlah.add(jumlah);
                }
                c.moveToNext();
            }
        }
        Log.e("NAME GRUP", "" + listGrup.size());
        Log.e("NAME ID", "" + listIdGrup.size());
        Log.e("NAME JUMLAH", "" + listJumlah.size());
    }

    void sync(String title) {
        if (listGrup.size() == 0){
            addItem(title);
        } else {
            if (listGrup.contains(title)){
                Toast.makeText(this, "Nama Grup Sudah Ada", Toast.LENGTH_SHORT).show();
            }else {
                addItem(title);
            }
        }

    }

    void dialogAdd() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewInflater = inflater.inflate(R.layout.dialog_add, (ViewGroup) findViewById(android.R.id.content), false);
        final EditText et_title = (EditText) viewInflater.findViewById(R.id.et_add_title);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Masukkan Judul Random")
                .setView(viewInflater)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (et_title.length() == 0) {
                            Toast.makeText(ActivityMain.this, "Masukkan judul", Toast.LENGTH_SHORT).show();
                        } else {
                            String title = et_title.getText().toString();
//                            Toast.makeText(ActivityMain.this, title, Toast.LENGTH_SHORT).show();

                            sync(title);

                            dialog.cancel();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    void dialogDelete(final String title, final int position){
        new AlertDialog.Builder(this)
                .setTitle("Delete Item").setMessage("Are you sure want to delete this item?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeItem(title, position);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    void dialogInfo(){
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewInflater = inflater.inflate(R.layout.dialog_info, (ViewGroup) findViewById(android.R.id.content), false);
//        final TextView tv_info = (TextView) viewInflater.findViewById(R.id.tv_info);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Aplikasi dibuat oleh")
                .setView(viewInflater)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create().show();
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (isLongClick){
            dialogDelete(listIdGrup.get(position), position);
        }else {
            Intent i = new Intent(ActivityMain.this, ActivityMember.class);
            i.putExtra("Grup", listGrup.get(position));
            i.putExtra("Id_grup", listIdGrup.get(position));
            startActivity(i);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tab,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_info){
            dialogInfo();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda yakin ingin menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
}
